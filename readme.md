REST/HTML/JavaScript Web Application with MongoDB+JAX-RS+AngularJS
==================================================================
This is a sample web application that uses mongodb for storage, JAX-RS
(Jersey) for RESTful service backend, and AnjguarJS+HTML+JS as SPA
(single-page-application) front-end.

How to run
----------
- Install [JDK 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
- Install [Maven 3](http://maven.apache.org/download.cgi)
- Install [MongoDB](http://www.mongodb.org/downloads)
- Run `mvn jetty:run`
- Point your browser to [http://localhost:8080/](http://localhost:8080/)
- Read the instructions at the top of the page on how to use the application in browser or with `curl`

The above assumes that both Jetty and MongoDB are listening on their default ports.

Why?
----
This code base can serve as a prototype for your own applications. Just take whatever your find useful. Discard the rest.

- On server-side, [JAX-RS](http://jersey.java.net/) makes it very easy to expose REST services that consume JSON and produce JSON.
- On client-side, [AngularJS](http://angularjs.org/) makes it very easy to two-way bind JavaScript Model objects to the HTML UI, send them to server as JSON over HTTP, and receive JSON over HTTP responses and convert them back into the JavaScript Model to be displayed in the View.
- [Genson](https://code.google.com/p/genson/) automatically converts HTTP request payload from JSON to POJO and HTTP response payload from POJO to JSON. Zero line of code. Zero lines of configuration.
- No ORM needed. Forget Hibernate, MongoDB is itself a document store. You persist your Java objects directly to MongoDB as BSON documents. And the conversion is automatic, thanks to [MongoJack](http://mongojack.org/).

Understanding and using the code
--------------------------------
- To make it even easier, the project includes base classes for your persistent entities (POJO's saved to MongoDB) and the REST resources (JAX-RS controllers with @Path annotation)
- [EntityBase](https://bitbucket.org/jitesh_doshi/mongorest/src/master/src/main/java/com/spinspire/mongorest/persistence/EntityBase.java?at=master) is the class you should derive your persistent Java classes from. E.g. [Hello](https://bitbucket.org/jitesh_doshi/mongorest/src/master/src/main/java/com/spinspire/mongorest/model/Hello.java?at=master). The derived class simply includes public fields or public accessor methods to make them persistent.
- [MongoResource](https://bitbucket.org/jitesh_doshi/mongorest/src/master/src/main/java/com/spinspire/mongorest/persistence/MongoResource.java?at=master) is the class you should derive your JAX-RS resource/controller from. E.g. [HelloResource](https://bitbucket.org/jitesh_doshi/mongorest/src/master/src/main/java/com/spinspire/mongorest/resource/HelloResource.java?at=master). The derived class inherits all the HTTP REST verbs (GET, POST, PUT, DELETE) automatically, but it also has the option to override any of them, or add new ones.
- Take a look at [web.xml](https://bitbucket.org/jitesh_doshi/mongorest/src/master/src/main/webapp/WEB-INF/web.xml?at=master) for how the JAX-RS controller servlet is mapped to /api/* path, which makes all JAX-RS resources accessible under that path, such as /api/hello.

To see what's going on in the database, connect to mongodb using `mongo` command-line client ...

    $ mongo
    > use test;
    > db.hello.find();
    { "_id" : ObjectId("520fc84030f7fbc7f8d8651c"), "greeting" : "hello", "name" : "jitesh" }
    { "_id" : ObjectId("520fc85530f7fbc7f8d8651f"), "greeting" : "hiya", "name" : "doshi" }