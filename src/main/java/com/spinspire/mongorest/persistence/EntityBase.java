package com.spinspire.mongorest.persistence;

import com.owlike.genson.annotation.JsonProperty;
import org.mongojack.Id;
import org.mongojack.ObjectId;

public class EntityBase {
  @Id
  @JsonProperty("_id")
  @ObjectId
  public String id;
}
