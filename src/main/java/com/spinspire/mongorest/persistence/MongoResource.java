package com.spinspire.mongorest.persistence;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.mongojack.DBSort;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

// @Consumes("application/json") // not needed
// @Produces("application/json") // not needed
public class MongoResource<T> {

  private JacksonDBCollection<T, String> coll;

  public MongoResource(Class<T> entityType, String collectionName) {
    this.coll = JacksonDBCollection.wrap(Storage.db.getCollection(collectionName), entityType, String.class);
  }

  @GET
  public List<T> list() {
    List<T> result = new ArrayList<>();
    for (T o : coll.find().sort(DBSort.desc("_id"))) {
      result.add(o);
    }
    return result;
  }

  @GET
  @Path("/{id}")
  public T get(@PathParam("id") String id) {
    T record = coll.findOneById(id);
    if (record == null) {
      throw new NotFoundException();
    }
    return record;
  }

  @POST
  public String insert(T hello) {
    WriteResult<T, String> result = coll.insert(hello);
    return result.getSavedId();
  }

  @PUT
  @Path("/{id}")
  public int update(@PathParam("id") String id, T hello) {
    WriteResult<T, String> result = coll.updateById(id, hello);
    return result.getN();
  }

  @DELETE
  @Path("/{id}")
  public int delete(@PathParam("id") String id) {
    WriteResult<T, String> result = coll.removeById(id);
    return result.getN();
  }
}
