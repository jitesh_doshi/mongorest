package com.spinspire.mongorest.persistence;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;

public class Storage {
  public static final MongoClient mongoClient;
  public static final DB db;

  static {
    try {
      mongoClient = new MongoClient();
      db = mongoClient.getDB("test");
    } catch (UnknownHostException ex) {
      throw new RuntimeException(ex);
    }

  }
}
