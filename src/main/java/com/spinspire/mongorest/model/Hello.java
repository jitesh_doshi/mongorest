/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spinspire.mongorest.model;

import com.spinspire.mongorest.persistence.EntityBase;

public class Hello extends EntityBase {
  private static final long serialVersionUID = 1L;
  public String greeting;
  public String name;

  public Hello() {
    this(null, null);
  }

  public Hello(String greeting, String name) {
    this.greeting = greeting;
    this.name = name;
  }

  @Override
  public String toString() {
    return greeting + " " + name;
  }
}
