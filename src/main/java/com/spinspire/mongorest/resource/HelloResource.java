package com.spinspire.mongorest.resource;

import com.spinspire.mongorest.model.Hello;
import com.spinspire.mongorest.persistence.MongoResource;
import javax.ws.rs.Path;

/**
 * A sample resource controller that is derived from
 * <code>MongoResource</code>. It specifies only the entity class it controls and the mongo collection name it uses, while
 * inheriting all CRUD operations.
 *
 * @see MongoResource
 */
@Path("/hello")
public class HelloResource extends MongoResource<Hello> {

  public HelloResource() {
    super(Hello.class, "hello");
  }
}
